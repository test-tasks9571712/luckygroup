export class API {
	static base_url  = 'https://baconipsum.com/api/';

	static async fetchData(params) {
		const request = await fetch(this.base_url + '?' + params);
		if (request.ok) {
			return request.json();
		}
	}

	static async attachContent(targetElement, params) {
		const content = await this.fetchData(params);
		if (content) {
			targetElement.textContent = content;
		}
	}
}